public class Domino{

	private int p;	//mulige daekninger
	private int n;	//laengde

	public Domino11(int n) {
		p = 0;
		this.n = n;
	}

	private void findP(int n){		//langde = n, bredde = 2
		if (n == 0){
			p++;			//naar n er 0, er fladen daekket, og vi har saaledes fundet en mulighed
		}
		if (n > 0) {
			findP(n-1);		//en vertikal brik
		}
		if (n > 1) {
			findP(n-2);		//to horizontale brikker
		}
	}

	public int getP() {
		p = 0;
		findP(n);
		return p;
	}

	public static void main(String[] args){
		System.out.println(new Domino11(20).getP());	//test: hvor mange muligheder er der ved laengde 20?
	}
}
